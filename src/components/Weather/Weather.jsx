import React, { Component } from "react";
import './Weather.scss';
import Slider from "react-slick";
import Day from '../Day/Day.jsx';


class Weather extends Component {

    dataWeather = () => {
        let day = [];
        let week = [];
        Object.keys(this.props.data).map((item) => {
            const today = 1485853200417;
            if (item == 'list') {
                let i = 0;
                this.props.data[item].map((item) => {
                    if (item.dt * 1000 > today) {
                        const key = item.dt;
                        const week_day = (i === 0) ? 'Сегодня' : this.props.week[new Date(item.dt * 1000).getDay()];
                        const date = new Date(item.dt * 1000).getDate();
                        const month = this.props.months[new Date(item.dt * 1000).getMonth()];
                        const icon = 'http://openweathermap.org/img/w/' + `${item.weather[0].icon}` + '.png';
                        const temp_day = Math.round(item.temp.day - 273.15);
                        const temp_night = Math.round(item.temp.night - 273.15);
                        const weather_description = item.weather[0].description;
                        i++;
                        day = { week_day, date, month, icon, temp_day, temp_night, weather_description, key };
                        week.push(day);
                    }
                })
            }
        })
        return week;
    }

    handleWeather = () => {
        return this.dataWeather().map((item) => {
            return <Day key={item.key} week_day={item.week_day}
                date={item.date}
                month={item.month}
                src={item.icon}
                temp_day={item.temp_day}
                temp_night={item.temp_night}
                weather_description={item.weather_description} />
        })

    }

    render() {
        const settings = {
            arrows: true,
            infinite: false,
            speed: 700,
            slidesToShow: 4,
            slidesToScroll: 1,
            className: 'slides'
        };
        return (
            <Slider {...settings}>
                {this.handleWeather()}
            </Slider>

        )
    }
}

export default Weather;