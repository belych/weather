import React, { Component } from "react";
import './Day.scss';

class Day extends Component {
    render() {
        const { week_day, date, month, src,
            temp_day, temp_night, weather_description } = this.props;
        return (
            <ul className='weather__item weather-item'>
                <li className='weather-item__week_day'>{week_day}</li>
                <li className='weather-item__date'>{date} {month}</li>
                <li className='weather-item__img'><img src={src} /></li>
                <li className='weather-item__temp_day weather-item-temp_day'>
                    <div className='weather-item-temp_day__text'>
                        <span>днем {temp_day}</span>
                        <img src='../public/images/null.png'
                            className='weather-item-temp_day__img' />
                    </div></li>
                <li className='weather-item__temp_night weather-item-temp_night'>
                    <div className='weather-item__temp_night__text'>
                        <span>ночью {temp_night}</span>
                        <img src='../public/images/null.png'
                            className='weather-item__temp_night__img' />
                    </div></li>
                <li className='weather-item__weather_description'>{weather_description}</li>
            </ul>
        )
    }
}

export default Day;