import React, { Component } from "react";
import Weather from './components/Weather/Weather.jsx';
import data from '../public/data/data';
import months from '../public/data/months';
import week from '../public/data/week';

class App extends Component {

    render() {
        return (
            <div className='weather'>
                <div className='weather__header'>Прогноз погоды</div>
                <div className='weather__list'>
                    <Weather data={data} months={months} week={week} />
                </div>
            </div>
        );
    }
}

export default App;